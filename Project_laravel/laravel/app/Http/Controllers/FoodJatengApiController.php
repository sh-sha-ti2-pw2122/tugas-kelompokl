<?php

namespace App\Http\Controllers;

use App\FoodJateng;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class FoodjatengApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $food_jateng = FoodJateng::all()->toJson(JSON_PRETTY_PRINT);
        return response($food_jateng, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'nama' => 'required',
            'harga' => 'required',
            'image' => 'required|file|image|max:2000',
            'desc' => 'required',
            'toko' => 'required',
            'buka' => 'required',
            'no_telp' => 'required',
        ]);
        if ($validateData->fails()) {
            return response($validateData->errors(), 400);
        } else {
            $food_jateng = new FoodJateng();
            $food_jateng->nama = $request->nama;
            $food_jateng->harga = $request->harga;
            if ($request->hasFile('image')) {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                $path = $request->image->move('assets/img/makanan', $namaFile);
                $food_jateng->image = $path;
            }
            $food_jateng->desc = $request->desc;
            $food_jateng->toko = $request->toko;
            $food_jateng->buka = $request->buka;
            $food_jateng->no_telp = $request->no_telp;
            $food_jateng->timestamps = false;
            $food_jateng->timestamps = false;
            $food_jateng->save();
            return response()->json(["message" => "food jateng record created"], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (FoodJateng::where('id', $id)->exists()) {
            $validateData = Validator::make($request->all(), [
                'nama' => 'required',
                'harga' => 'required',
                'image' => 'required|file|image|max:2000',
                'desc' => 'required',
                'toko' => 'required',
                'buka' => 'required',
                'no_telp' => 'required',
            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
            } else {
                $food_jateng = FoodJateng::find($id);
                $food_jateng->nama = $request->nama;
                $food_jateng->harga = $request->harga;
                if ($request->hasFile('image')) {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-' . time() . "." . $extFile;
                    $path = $request->image->move('assets/img/makanan', $namaFile);
                    $food_jateng->image = $path;
                }
                $food_jateng->desc = $request->desc;
                $food_jateng->toko = $request->toko;
                $food_jateng->buka = $request->buka;
                $food_jateng->no_telp = $request->no_telp;
                $food_jateng->timestamps = false;
                $food_jateng->timestamps = false;
                $food_jateng->save();
                return response()->json(["message" => "food jateng record updated"], 201);
            }
        } else {
            return response()->json(["message" => "Food jateng not found"], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (FoodJateng::where('id', $id)->exists()) {
            $food_jateng = FoodJateng::find($id);
            File::delete($food_jateng->image);
            $food_jateng->delete();
            return response()->json(["message" => "food jateng record deleted"], 201);
        } else {
            return response()->json(["message" => "Food Jateng not found"], 404);
        }
    }
}
