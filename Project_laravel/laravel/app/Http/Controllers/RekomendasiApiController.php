<?php

namespace App\Http\Controllers;

use App\Rekomendasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RekomendasiApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'nama' => 'required',
            'jenis' => 'required',
            'harga' => 'required',
            'no_telp' => 'required',
            'image' => 'required|file|image|max:2000',
        ]);
        $rekomendasi = new Rekomendasi();
        $rekomendasi->nama = $validateData->nama;
        $rekomendasi->jenis = $validateData->jenis;
        $rekomendasi->harga = $validateData->harga;
        $rekomendasi->no_telp = $validateData->no_telp;
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            $path = $request->image->move('assets/img/makanan', $namaFile);
            $rekomendasi->image = $path;
        }
        $rekomendasi->timestamps = false;
        $rekomendasi->save();
        return response()->json(["message" => "Rekomendasi record created"], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Rekomendasi::where('id', $id)->exists()) {
        $validateData = Validator::make($request->all(), [
            'nama' => 'required',
            'jenis' => 'required',
            'harga' => 'required',
            'no_telp' => 'required',
            'image' => 'required|file|image|max:2000',
        ]);
        $rekomendasi = Rekomendasi::find($id);
        $rekomendasi->nama = $validateData->nama;
        $rekomendasi->jenis = $validateData->jenis;
        $rekomendasi->harga = $validateData->harga;
        $rekomendasi->no_telp = $validateData->no_telp;
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            $path = $request->image->move('assets/img/makanan', $namaFile);
            $rekomendasi->image = $path;
        }
        $rekomendasi->timestamps = false;
        $rekomendasi->save();
        return response()->json(["message" => "Rekomendasi record created"], 201);
    } else {
        return response()->json(["message" => "Rekomendasi not found"], 404);
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
