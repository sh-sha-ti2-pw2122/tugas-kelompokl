<?php

namespace App\Http\Controllers;

use App\FoodJatim;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class FoodJatimApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $food_jatim = FoodJatim::all()->toJson(JSON_PRETTY_PRINT);
        return response($food_jatim, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'nama' => 'required',
            'harga' => 'required',
            'image' => 'required|file|image|max:2000',
            'desc' => 'required',
            'toko' => 'required',
            'buka' => 'required',
            'no_telp' => 'required',
        ]);
        if ($validateData->fails()) {
            return response($validateData->errors(), 400);
        } else {
            $food_jatim = new FoodJatim();
            $food_jatim->nama = $request->nama;
            $food_jatim->harga = $request->harga;
            if ($request->hasFile('image')) {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                $path = $request->image->move('assets/img/makanan', $namaFile);
                $food_jatim->image = $path;
            }
            $food_jatim->desc = $request->desc;
            $food_jatim->toko = $request->toko;
            $food_jatim->buka = $request->buka;
            $food_jatim->no_telp = $request->no_telp;
            $food_jatim->timestamps = false;
            $food_jatim->timestamps = false;
            $food_jatim->save();
            return response()->json(["message" => "food jatim record created"], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (FoodJatim::where('id', $id)->exists()) {
            $validateData = Validator::make($request->all(), [
                'nama' => 'required',
                'harga' => 'required',
                'image' => 'required|file|image|max:2000',
                'desc' => 'required',
                'toko' => 'required',
                'buka' => 'required',
                'no_telp' => 'required',
            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
            } else {
                $food_jatim = FoodJatim::find($id);
                $food_jatim->nama = $request->nama;
                $food_jatim->harga = $request->harga;
                if ($request->hasFile('image')) {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-' . time() . "." . $extFile;
                    $path = $request->image->move('assets/img/makanan', $namaFile);
                    $food_jatim->image = $path;
                }
                $food_jatim->desc = $request->desc;
                $food_jatim->toko = $request->toko;
                $food_jatim->buka = $request->buka;
                $food_jatim->no_telp = $request->no_telp;
                $food_jatim->timestamps = false;
                $food_jatim->timestamps = false;
                $food_jatim->save();
                return response()->json(["message" => "food jatim record updated"], 201);
            }
        } else {
            return response()->json(["message" => "Food jatim not found"], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (FoodJatim::where('id', $id)->exists()) {
            $food_jatim = FoodJatim::find($id);
            File::delete($food_jatim->image);
            $food_jatim->delete();
            return response()->json(["message" => "food jatim record deleted"], 201);
        } else {
            return response()->json(["message" => "Food Jatim not found"], 404);
        }
    }
}
