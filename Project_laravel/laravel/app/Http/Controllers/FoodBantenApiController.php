<?php

namespace App\Http\Controllers;

use App\FoodBanten;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class FoodBantenApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $food_banten = FoodBanten::all()->toJson(JSON_PRETTY_PRINT);
        return response($food_banten, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'nama' => 'required',
            'harga' => 'required',
            'image' => 'required|file|image|max:2000',
            'desc' => 'required',
            'toko' => 'required',
            'buka' => 'required',
            'no_telp' => 'required',
        ]);
        if ($validateData->fails()) {
            return response($validateData->errors(), 400);
        } else {
            $food_banten = new FoodBanten();
            $food_banten->nama = $request->nama;
            $food_banten->harga = $request->harga;
            if ($request->hasFile('image')) {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                $path = $request->image->move('assets/img/makanan', $namaFile);
                $food_banten->image = $path;
            }
            $food_banten->desc = $request->desc;
            $food_banten->toko = $request->toko;
            $food_banten->buka = $request->buka;
            $food_banten->no_telp = $request->no_telp;
            $food_banten->timestamps = false;
            $food_banten->timestamps = false;
            $food_banten->save();
            return response()->json(["message" => "food banten record created"], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (FoodBanten::where('id', $id)->exists()) {
            $validateData = Validator::make($request->all(), [
                'nama' => 'required',
                'harga' => 'required',
                'image' => 'required|file|image|max:2000',
                'desc' => 'required',
                'toko' => 'required',
                'buka' => 'required',
                'no_telp' => 'required',
            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
            } else {
                $food_banten =FoodBanten::find($id);
                $food_banten->nama = $request->nama;
                $food_banten->harga = $request->harga;
                if ($request->hasFile('image')) {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-' . time() . "." . $extFile;
                    $path = $request->image->move('assets/img/makanan', $namaFile);
                    $food_banten->image = $path;
                }
                $food_banten->desc = $request->desc;
                $food_banten->toko = $request->toko;
                $food_banten->buka = $request->buka;
                $food_banten->no_telp = $request->no_telp;
                $food_banten->timestamps = false;
                $food_banten->timestamps = false;
                $food_banten->save();
                return response()->json(["message" => "food banten record updated"], 201);
            }
        } else {
            return response()->json(["message" => "Food Banten not found"], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (FoodBanten::where('id', $id)->exists()) {
            $food_banten = FoodBanten::find($id);
            File::delete($food_banten->image);
            $food_banten->delete();
            return response()->json(["message" => "food banten record deleted"], 201);
        } else {
            return response()->json(["message" => "Food Banten not found"], 404);
        }
    }
}
