<?php

namespace App\Http\Controllers;

use App\FoodJabar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class FoodJabarApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $food_jabar = FoodJabar::all()->toJson(JSON_PRETTY_PRINT);
        return response($food_jabar, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'nama' => 'required',
            'harga' => 'required',
            'image' => 'required|file|image|max:2000',
            'desc' => 'required',
            'toko' => 'required',
            'buka' => 'required',
            'no_telp' => 'required',
        ]);
        if ($validateData->fails()) {
            return response($validateData->errors(), 400);
        } else {
            $food_jabar = new FoodJabar();
            $food_jabar->nama = $request->nama;
            $food_jabar->harga = $request->harga;
            if ($request->hasFile('image')) {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                $path = $request->image->move('assets/img/makanan', $namaFile);
                $food_jabar->image = $path;
            }
            $food_jabar->desc = $request->desc;
            $food_jabar->toko = $request->toko;
            $food_jabar->buka = $request->buka;
            $food_jabar->no_telp = $request->no_telp;
            $food_jabar->timestamps = false;
            $food_jabar->timestamps = false;
            $food_jabar->save();
            return response()->json(["message" => "food jabar record created"], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (FoodJabar::where('id', $id)->exists()) {
            $validateData = Validator::make($request->all(), [
                'nama' => 'required',
                'harga' => 'required',
                'image' => 'required|file|image|max:2000',
                'desc' => 'required',
                'toko' => 'required',
                'buka' => 'required',
                'no_telp' => 'required',
            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
            } else {
                $food_jabar = FoodJabar::find($id);
                $food_jabar->nama = $request->nama;
                $food_jabar->harga = $request->harga;
                if ($request->hasFile('image')) {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-' . time() . "." . $extFile;
                    $path = $request->image->move('assets/img/makanan', $namaFile);
                    $food_jabar->image = $path;
                }
                $food_jabar->desc = $request->desc;
                $food_jabar->toko = $request->toko;
                $food_jabar->buka = $request->buka;
                $food_jabar->no_telp = $request->no_telp;
                $food_jabar->timestamps = false;
                $food_jabar->timestamps = false;
                $food_jabar->save();
                return response()->json(["message" => "food jabar record updated"], 201);
            }
        } else {
            return response()->json(["message" => "Food Jabar not found"], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (FoodJabar::where('id', $id)->exists()) {
            $food_jabar = FoodJabar::find($id);
            File::delete($food_jabar->image);
            $food_jabar->delete();
            return response()->json(["message" => "food jabar record deleted"], 201);
        } else {
            return response()->json(["message" => "food jabar not found"], 404);
        }
    }
}
