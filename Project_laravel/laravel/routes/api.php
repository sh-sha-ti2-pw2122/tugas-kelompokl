<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//api foodBanten
Route::post('food_banten', 'FoodBantenApiController@store');
Route::get('food_banten', 'FoodBantenApiController@index');
Route::put('food_banten/{id}', 'FoodBantenApiController@update');
Route::delete('food_banten/{id}', 'FoodBantenApiController@destroy');

//api jabar
Route::post('food_jabar', 'FoodJabarApiController@store');
Route::get('food_jabar', 'FoodJabarApiController@index');
Route::put('food_jabar/{id}', 'FoodJabarApiController@update');
Route::delete('food_jabar/{id}', 'FoodJabarApiController@destroy');

// api Jatim
Route::post('food_jatim', 'FoodJatimApiController@store');
Route::get('food_jatim', 'FoodJatimApiController@index');
Route::put('food_jatim/{id}', 'FoodJatimApiController@update');
Route::delete('food_jatim/{id}', 'FoodJatimApiController@destroy');

// api jateng
Route::post('food_jateng', 'FoodJatengApiController@store');
Route::get('food_jateng', 'FoodJatengApiController@index');
Route::put('food_jateng/{id}', 'FoodJatengApiController@update');
Route::delete('food_jateng/{id}', 'FoodJatengApiController@destroy');

// api rekomendasi
Route::post('rekomendasi', 'RekomendasiApiController@store');
Route::get('rekomendasi', 'RekomendasiApiController@index');
Route::put('rekomendasi/{id}', 'RekomendasiApiController@update');
Route::delete('rekomendasi/{id}', 'RekomendasiApiController@destroy');
